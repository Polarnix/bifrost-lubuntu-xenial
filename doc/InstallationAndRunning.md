# Installing and Running Bifrost #

## Introduction ##

Bifrost runs within a server and, one or more, client virtual machines.  The server supplies satellite data processing, ice chart map generation and dissemination, and database services.  These are used by the clients, typically through a GIS workstation environment based on [QGIS](http://www.qgis.org/en/site/).

## Prerequisites ##

Oracle [VirtualBox](https://www.virtualbox.org/) and HashiCorp [Vagrant](https://www.vagrantup.com/) are required to run the virtual machines.  Installation is also assisted by [Ansible](https://www.ansible.com/) but since this unavailable for Windows platforms it is supllied with and run inside the virtual machines.

The virtual machines currently run lightweight 64-bit Linux [Lubuntu](http://lubuntu.net/) 16.04 Xenial and provide a user interface to the Bifrost system.

Each virtual machine can use a maximum of 64 Gb disk space.  The server is set to use 2 CPU cores and 2 Gb memory, whilst the client uses 1 CPU core and 1 Gb memory.  The memory limits can be adjusted in the *Vagrantfile*, see Steps 4 and 5.  The virtual machines will dynamically allocate disk space up to the 64 Gb limit, so initially usage will be much less.  A system with 4 CPU cores, 128 Gb disk space and 4 Gb memory will be capable of running Bifrost. 

### Step 1 - Install VirtualBox ###

See the [Downloads](https://www.virtualbox.org/wiki/Downloads) page and download the respective install package for your computer. Also download the Extension Pack from the same page.

The Extension Pack provides additional functionality and can be installed by going to the *File* drop-down menu, selecting *Preferences*.  From the *Preferences* window, select the *Extensions* tab, and then the *Add new package* icon (blue square, with downward yellow arrow).

![virtualbox_extensions.png](/doc/graphics/virtualbox_extensions.png)

### Step 2 - Install Vagrant ###

See the [Downloads](https://www.vagrantup.com/downloads.html) page and download the respective install package for your computer.

Once installed, the [Vagrant Host Manager](https://github.com/devopsgroup-io/vagrant-hostmanager) plugin is also required.  To install this, open a Terminal window on Linux or Mac computers (PowerShell on Windows) and enter the following command:

`vagrant plugin install vagrant-hostmanager`

This downloads and installs the plugin to Vagrant.

## Installing Bifrost ##

### Step 3 - Download the Vagrant box package ###

The Vagrant box package can be found at [https://bitbucket.org/Polarnix/bifrost-lubuntu-xenial](https://bitbucket.org/Polarnix/bifrost-lubuntu-xenial).  If you are familiar with [Git](https://git-scm.com/) you can clone the repository using the link at the top of the page or, if you do not have Git installed, select *Downloads* from the menu list on the left side of the page.  You can then click on *Download repository* to get a zip-file which then has to be uncompressed.

### Step 4 - Install the Bifrost server ###

From the files in the Git repository, or extracted from the zip-file, locate the directory containing the one called *Vagrantfile*.

Open a Terminal window (Linux or Mac) or PowerShell (Windows) and change directory to the location of the *Vagrantfile*.

Enter the command:

`vagrant up biserver`

After unpacking the Vagrant box, a window containing the Bifrost server login is generated.

![vagrant_login_window.png](/doc/graphics/vagrant_login_window.png)

Meanwhile, in the Terminal window the installation will continue as the [Bifrost software](https://bitbucket.org/Polarnix/bifrost) is copied to the virtual machine and Ansible is run to set up user accounts, install software, and create the databases.  This part **will** take some time...

Various supporting data, such as coastlines from [NOAA GSHHG](https://www.ngdc.noaa.gov/mgg/shorelines/gshhs.html) and [SCAR ADD](http://www.add.scar.org/), bathymetry from [IBCAO](https://www.ngdc.noaa.gov/mgg/bathymetry/arctic/arctic.html), are downloaded and installed.

If the process in the Terminal window ends with anything that looks like an error, the installation process can be restarted by:

`vagrant provision biserver`

### Step 5 - Install the Bifrost client ###

From the files in the Git repository, or extracted from the zip-file, locate the directory containing the one called *Vagrantfile*.

Open a Terminal window (Linux or Mac) or PowerShell (Windows) and change directory to the location of the *Vagrantfile*.

Enter the command:

`vagrant up biclient`

After unpacking the Vagrant box, a window containing the Bifrost client login is generated.

![Login window](/graphics/vagrant_login_window.png)

If the process in the Terminal window ends with anything that looks like an error, the installation process can be restarted by:

`vagrant provision biclient`


## Shutting down the Bifrost virtual machines ##

Use:

`vagrant halt biserver`

and

`vagrant halt biclient`

respectively.

Any files and data created will be stored on disk.
