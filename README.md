![bifrost_logo.png](/resources/bifrost_logo.png){width=200}

Bifrost is the ice charting system of the [Norwegian Ice Service](http://www.met.no/).  This repository contains the [Ansible](https://www.ansible.com/) scripts that create  a working copy of the system as a server-client pair of [Vagrant](https://www.vagrantup.com/) boxes, running under [VirtualBox](https://www.virtualbox.org/).

# Installation and Running #

As Bifrost works under VirtualBox, it is capable of being deployed on any operating system that VirtualBox runs on.  See the [Installation Guide](/doc/InstallationAndRunning.md) for more information on installing and running Bifrost.

Due to the large size of the Vagrant box file used for Bifrost, the repository is split into 2 parts. You can find the Ansible set up files and scripts that are installed to the virtual machines at the [Bifrost repository](https://bitbucket.org/Polarnix/bifrost).

### Note ###

The scripts here are copies of those used on the operational ice charting system of the Norwegian Ice Service. On this version of Bifrost they are very much work in progress and there may be some bugs or things that do not work as expected.  The intention with Bifrost is to develop these into icons that can be integrated into the QGIS menu system.

**Citing this software**  
This software has DOI: 10.5281/zenodo.884048 on Zenodo.  
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.884048.svg)](https://doi.org/10.5281/zenodo.884048)

